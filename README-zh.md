# 圓石+ 圖示包  Stone Plus Iconpack

<p align="center">
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/iconpack.jpg" height="220"/>
</p>

---
Copyright: 一個源自 A Little Design 的新項目.💎 An Icon Design.🎨[![GitHub stars](https://img.shields.io/github/stars/a-Little-Design/stone-plus-iconpack.svg?style=social&label=Star)](https://github.com/a-Little-Design/stone-plus-iconpack) [關於 A Little Design.](https://github.com/a-Little-Design)

---
---
---

# 下載 - 圖示包 APK檔案
- [軟體下載。💎](https://gitlab.com/an-icon-design/stone-plus-iconpack/-/releases)

---
---
---


:blue_heart: 一款基於 Blueprint 軟體框架製作的免費圖示包。 [![GitHub stars](https://img.shields.io/github/stars/jahirfiquitiva/Blueprint.svg?style=social&label=Star)](https://github.com/jahirfiquitiva/Blueprint)
[關於 Blueprint.](https://github.com/jahirfiquitiva/Blueprint)

---
---
---

<p align="center">
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/Promo_stone_plus_1.webp"/>
</p>

<p align="center">
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/Promo_stone_plus_2.webp"/>
</p>

<p align="center">
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/Promo_stone_plus.webp"/>
</p>

---
---
---

# Features: :radio_button: 
- 遵循 Material Design 設計的軟體界面。🎨
- 內置“申請適配圖示”的功能組件。💻
- 支持多達 20 款安卓第三方桌面應用軟體。✨
- 支持雲端下載手機桌布功能。🎉

# About Codes: :page_with_curl:
- 基於 Blueprint v2.3.4 版本代碼製作。💎
- 刪減部分不必要的代碼和組件。🔨
- 改進和優化部分代碼。🔧

# Licenses💎
# Source Code Licenses
This Source Code is shared under the [CreativeCommons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) License](https://creativecommons.org/licenses/by-sa/4.0).

	Copyright © 2023 Icon Design

	Licensed under the CreativeCommons Attribution-ShareAlike 4.0 International 
	(CC BY-SA 4.0) License. You may not use this file except in compliance 
	with the License. You may obtain a copy of the License at

	   https://creativecommons.org/licenses/by-sa/4.0/legalcode
	   
           https://creativecommons.org/licenses/by-sa/4.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
- 本項目的代碼程式遵循上述許可協議進行免費開源。
- 本項目的代碼程式爲定製優化版本。若要獲取最新版本， 請查看原版 [Blueprint](https://github.com/jahirfiquitiva/Blueprint) 代碼庫。


# Application Licenses
This Application file which the package of APK, is shared under the [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) License](https://creativecommons.org/licenses/by-nc-sa/4.0).

	Copyright © 2023 Icon Design

	Licensed under the CreativeCommons Attribution-NonCommercial-ShareAlike 4.0 
	International (CC BY-NC-SA 4.0) License. You may not use this file except in 
	compliance with the License. You may obtain a copy of the License at

	   https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode
	   
           https://creativecommons.org/licenses/by-nc-sa/4.0/

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
- 本項目Release中提供的APK軟體程式，只作爲免費軟體分發和許可。請勿用於商業領域。
- 本軟體APK文件的Copyright權利歸屬軟件開發者所有。



# Preview🎁
<p align="center">
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/Play%20(1).jpg" height="350"/>
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/Play%20(2).jpg" height="350"/>
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/Play%20(3).jpg" height="350"/>
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/Play%20(4).jpg" height="350"/>
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/Play%20(5).jpg" height="350"/>
</p>

<p align="center">
<img src="https://cdn.jsdelivr.net/gh/a-Little-Design/hello/cloud/app/src/main/res/promo/stone_plus_promo.png" height="220"/>
</p>


#
#
#
#
---
#
#
#
# 
---


# 圆石+ 图标包  Stone Plus Iconpack

---
Copyright: 一个源自 A Little Design 的新项目.💎 An Icon Design.🎨[![GitHub stars](https://img.shields.io/github/stars/a-Little-Design/stone-plus-iconpack.svg?style=social&label=Star)](https://github.com/a-Little-Design/stone-plus-iconpack) [关于 A Little Design.](https://github.com/a-Little-Design)

---
---
---

# 下载 - 图标包 APK文件
- [软件下载。💎](https://gitlab.com/an-icon-design/stone-plus-iconpack/-/releases)

---
---
---

:blue_heart: 基于 Blueprint 软件框架制的免费图标包。 [![GitHub stars](https://img.shields.io/github/stars/jahirfiquitiva/Blueprint.svg?style=social&label=Star)](https://github.com/jahirfiquitiva/Blueprint)
[关于 Blueprint.](https://github.com/jahirfiquitiva/Blueprint)

<p align="center">
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/iconpack.jpg" height="220"/>
</p>

# Features: :radio_button: 
- 遵循 Material Design 设计的软件界面。🎨
- 内置“申请适配图标”的功能组件。💻
- 支持多达 20 款安卓第三方桌面应用软件。✨
- 支持云端下载手机壁纸功能。🎉

# About Codes: :page_with_curl:
- 基于 Blueprint v2.3.4 版本代码制作。💎
- 删减部分不必要代码和组件。🔨
- 改进和优化部分代码。🔧

# Licenses💎
# Source Code Licenses
This Source Code is shared under the [CreativeCommons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) License](https://creativecommons.org/licenses/by-sa/4.0).

	Copyright © 2023 Icon Design

	Licensed under the CreativeCommons Attribution-ShareAlike 4.0 International 
	(CC BY-SA 4.0) License. You may not use this file except in compliance 
	with the License. You may obtain a copy of the License at

	   https://creativecommons.org/licenses/by-sa/4.0/legalcode
	   
           https://creativecommons.org/licenses/by-sa/4.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
- 本项目的代码遵循上述许可协议进行免费开源。
- 本项目的代码为定制优化版本。若想获取最新版本， 请查看原版的 [Blueprint](https://github.com/jahirfiquitiva/Blueprint) 代码库。

# Application Licenses
This Application file which the package of APK, is shared under the [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) License](https://creativecommons.org/licenses/by-nc-sa/4.0).

	Copyright © 2023 Icon Design

	Licensed under the CreativeCommons Attribution-NonCommercial-ShareAlike 4.0 
	International (CC BY-NC-SA 4.0) License. You may not use this file except in 
	compliance with the License. You may obtain a copy of the License at

	   https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode
	   
           https://creativecommons.org/licenses/by-nc-sa/4.0/

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
- 本项目Release中提供的APK软件程序， 只作为免费软件分发和许可。请勿用于商业行为。
- 本软件APK文件的Copyright权利归软件开发者所有。


# Preview🎁
<p align="center">
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/Play%20(1).jpg" height="350"/>
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/Play%20(2).jpg" height="350"/>
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/Play%20(3).jpg" height="350"/>
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/Play%20(4).jpg" height="350"/>
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/Play%20(5).jpg" height="350"/>
</p>

<p align="center">
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/stone_plus_promo.png" height="220"/>
</p>

