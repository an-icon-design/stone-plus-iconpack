# Stone Plus Iconpack

<p align="center">
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/iconpack.jpg" height="220"/>
</p>

---
Copyright: A Little Design Project.💎 An Icon Design.🎨[![GitHub stars](https://img.shields.io/github/stars/a-Little-Design/stone-plus-iconpack.svg?style=social&label=Star)](https://github.com/a-Little-Design/stone-plus-iconpack) [A Little Design.](https://github.com/a-Little-Design)

---
---
---

:blue_heart: A Free iconpack App With Blueprint Android Dashboard. [![GitHub stars](https://img.shields.io/github/stars/jahirfiquitiva/Blueprint.svg?style=social&label=Star)](https://github.com/jahirfiquitiva/Blueprint)
[About Blueprint.](https://github.com/jahirfiquitiva/Blueprint)

---
---
---

<p align="center">
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/Promo_stone_plus_1.webp"/>
</p>

<p align="center">
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/Promo_stone_plus_2.webp"/>
</p>

<p align="center">
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/Promo_stone_plus.webp"/>
</p>

---
---
---

# [中文简介](https://gitlab.com/an-icon-design/stone-plus-iconpack/-/blob/main/README-zh.md)🎨
- [Click Here.✨](https://gitlab.com/an-icon-design/stone-plus-iconpack/-/blob/main/README-zh.md)

- [软件下载.💎](https://gitlab.com/an-icon-design/stone-plus-iconpack/-/releases)

# Features: :radio_button: 
- Material Design dashboard.🎨
- In-app Icon Requests tool.💻
- Apply section with 20 supported launchers.✨
- Cloud based Wallpapers.🎉

# About Codes: :page_with_curl:
- Based on Blueprint v2.3.4 version.💎
- Remove some module from original codes.🔨
- Improve some details.🔧

# Licenses💎
# Source Code Licenses
This Source Code is shared under the [CreativeCommons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) License](https://creativecommons.org/licenses/by-sa/4.0).

	Copyright © 2023 Icon Design

	Licensed under the CreativeCommons Attribution-ShareAlike 4.0 International 
	(CC BY-SA 4.0) License. You may not use this file except in compliance 
	with the License. You may obtain a copy of the License at

	   https://creativecommons.org/licenses/by-sa/4.0/legalcode
	   
           https://creativecommons.org/licenses/by-sa/4.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
  - The Source Code of this repository is free for open source use.
  - You can also get the last version source code from original [Blueprint](https://github.com/jahirfiquitiva/Blueprint) repository.
  
# Application Licenses
This Application file which the package of APK, is shared under the [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) License](https://creativecommons.org/licenses/by-nc-sa/4.0).

	Copyright © 2023 Icon Design

	Licensed under the CreativeCommons Attribution-NonCommercial-ShareAlike 4.0 
	International (CC BY-NC-SA 4.0) License. You may not use this file except in 
	compliance with the License. You may obtain a copy of the License at

	   https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode
	   
           https://creativecommons.org/licenses/by-nc-sa/4.0/

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
- The APK file of this application repository Release is only intended for free distribution and follow the [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) License](https://creativecommons.org/licenses/by-nc-sa/4.0). Not for commericial purposes.
- The Copyright of this Application's APK file belong to developer Icon Design.

# Preview🎁
<p align="center">
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/Play%20(1).jpg" height="350"/>
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/Play%20(2).jpg" height="350"/>
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/Play%20(3).jpg" height="350"/>
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/Play%20(4).jpg" height="350"/>
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/Play%20(5).jpg" height="350"/>

</p>
<p align="center">
<img src="https://gitlab.com/an-icon-design/hello/-/raw/main/cloud/app/src/main/res/promo/stone_plus_promo.png" height="220"/>
</p>
